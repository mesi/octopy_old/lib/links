import json
import logging
import re
from .base_link import BaseLink
from ..secop.datainfo import *

PRIORITY_VALUES = [0, 1, 2]


class RequestResponseLink(BaseLink):
    """

    get_reference (mandatory for a HWGW)
        if the link is to a serial or other device that handles the payload via one channel it needs to be able to refer
        to that payload. get_reference is the reference for reading out data from the device.
        for Epics it will be the pv name, for a serial device the parameter name.

    set_reference (mandatory for a writable HWGW)
        if the link is to a serial or other device that handles the payload via one channel it needs to be able to refer
        to that payload. set_reference is the reference for issuing a command or setting a parameter.
        for Epics it will be the pv name, for a serial device the parameter/command name.

    priority
        Priority is used by hardware gateways which has a limiting data transfer capacity(poll_intervall).
        0:  Highest priority, Should only be used by few, very important signals per device.
        1:  Default for writable, if the device is polled more often than pollintervall, writable signals should be
            prioritized.
        2:  Default for readables.

    """

    def __init__(self, link_dict):
        super().__init__(link_dict)
        self.value_dict = dict()

    def deserialize(self, link_dict):
        self.get_reference = link_dict.get('get reference')
        self.set_reference = link_dict.get('set reference')
        if self.set_reference:
            # Force link to be not readonly if a set-reference is present.
            link_dict['readonly'] = False
        super().deserialize(link_dict)
        self.skipped = False
        self.reply_regex = link_dict.get('hw reply regex')
        self.reply_regex_compiled = None
        if self.reply_regex:
            try:
                self.reply_regex_compiled = re.compile(self.reply_regex)
            except TypeError as err:
                raise secop_errors.InternalError(f"regex for link {self.value_topic} not valid: {err}")
        if not self.get_reference:
            raise secop_errors.InternalError(f"lib.Link: RequestResponseLink is missing mandatory key 'get reference' or it is empty: {link_dict}")
        if not self.readonly and not self.set_reference:
            raise secop_errors.InternalError(f"lib.Link: writable RequestResponseLink is missing mandatory key 'set reference' or it is empty: {link_dict}")
        if link_dict.get('priority') in PRIORITY_VALUES:
            self.priority = link_dict.get('priority')
        elif self.readonly:
            self.priority = 2
        else:
            self.priority = 1
        self.link_type = 'request response'

    def serialize(self):
        structure = super().serialize()
        structure['get reference'] = self.get_reference
        structure['set reference'] = self.set_reference
        structure['hw reply regex'] = self.reply_regex
        structure['priority'] = self.priority
        return structure

    def __eq__(self, other):
        """ RequestResponseLinks are equal if they have the same topics and references"""
        if self.value_topic != other.value_topic:
            return False
        if self.get_topic != other.get_topic:
            return False
        if self.set_topic != other.set_topic:
            return False
        if self.get_reference != other.get_reference:
            return False
        if self.set_reference != other.set_reference:
            return False
        return True

    def list_topics(self):
        """ List all topics in link"""
        ret = list()
        if self.value_topic:
            ret.append(self.value_topic)
        if self.set_topic:
            ret.append(self.set_topic)
        if self.get_topic:
            ret.append(self.get_topic)
        return ret

    def synthesize_command(self, inp, delimiter, separator=b' '):
        if not isinstance(inp, dict):
            if '{value}' in self.set_reference:
                inp = {'value': inp}
                out = self.value_dict
                out.update(inp)
                cmd = self.set_reference.format(**out).encode()
            else:
                cmd = self.set_reference.encode('utf-8') + separator + str(inp).encode('utf-8') + delimiter
        else:
            out = self.value_dict
            out.update(inp)
            try:
                out = self.datainfo.validate_value(out)
            except BaseException as err:
                print(err)
                return None
            cmd = self.set_reference.format(**out).encode()
        return cmd

    def parse_reply(self, rply):
        if rply == '':
            return None
        if isinstance(rply, bytes):
            rply = rply.decode()
        if isinstance(rply, str):
            rply = rply.strip()
            if '/r' in rply:
                rply.split('\r')
                print(('CR in recieved string, please correct line ending'))
                rply = rply[0].rstrip()
            elif '/r' in rply:
                rply.split('\n')
                print(('Newline in recieved string, please correct line ending'))
                rply = rply[0].rstrip()
        if self.reply_regex:
            try:
                rply = re.match(self.reply_regex, rply).groupdict()
            except TypeError:
                rply = None
            except Exception as err:
                print(f"Error parsing response '{rply}' with link regex '{str(self.reply_regex)}': {err}")
                return None
            if isinstance(rply, dict):
                if rply.get('value'):
                    rply = rply.get('value')
        if not isinstance(self.datainfo, DataInfo):
            self.set_datainfo(rply)
        try:
            rply = self.datainfo.validate_value(rply)
        except secop_errors.WrongTypeError as err:
            logging.error(err)
            return None
        if isinstance(rply, dict):
            self.value_dict.update(rply)
        return rply


if __name__ == '__main__':
    reply = b'100.00,50.00,0.00\r\n'
    reg_ex = '(?P < Kp >[\+\-]?[0-9.] *), (?P < Ki >[\+\-]?[0-9.] *), (?P < Kd >[\+\-]?[0-9.] *)'
    lakeseshore_336_PID_link = RequestResponseLink({'value topic': 'test1', 'get reference': 'test1_reference', 'hw reply regex': '(?P<p>[0-9.]*),(?P<i>[0-9.]*),(?P<d>[0-9.]*)'})
    print(lakeseshore_336_PID_link)
    link2 = RequestResponseLink({'value topic': 'test1', 'get reference': 'test1_reference', 'datainfo': {}})
    print(link2)
    link3 = RequestResponseLink({'value topic': 'test1', 'get reference': 'test1_reference'})
    print(lakeseshore_336_PID_link == link2)
    print(lakeseshore_336_PID_link == link3)
    lakseshore_336_typical_PID_replay_from_hardware = '+0050.0,+0020.0,+000.0'
    # print(link3.datainfo)
    # link3.datainfo = datainfo.DataInfo()
    # print(link3.datainfo)
    # print(link3.datainfo.structure())
    print('\n\n\n')
    print(link3)
    data = b'BUDAPEST       \r\n'
    data2 = 1.02
    print('reply is: {}'.format(lakeseshore_336_PID_link.parse_reply(reply)))
    print(lakeseshore_336_PID_link)
    p = r'{"value topic":"mercuryitc/pid/p","get topic":null,"set topic":"mercuryitc/pid/p/set","description":null,"readonly":false,"unit":null,"link type":"request response","channel id":"mercuryitc/pid/p","datasource id":"192.168.0.2:7020","datainfo":{"type":"double","min":null,"max":null,"unit":null,"absolute_resolution":null,"relative_resolution":null,"fmtstr":null},"get reference":"READ:DEV:MB1.T1:TEMP:LOOP:P","set reference":"SET:DEV:MB1.T1:TEMP:LOOP:P:{value}","hw reply regex":"STAT:(?:SET:)*DEV:MB1\\.T1:TEMP:LOOP:P:(?P<value>[0-9.]*)","priority":1}'
    pd = json.loads(p)
    pl = RequestResponseLink(pd)
    print(pl.synthesize_command('9'))