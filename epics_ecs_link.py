from .base_link import BaseLink
from ..secop import errors as secop_errors

EPICS_PV_NAME_SEPARATOR = ":"


class EpicsECSLink(BaseLink):
    def __init__(self, link_dict):
        self.get_reference = None
        self.set_reference = None
        super().__init__(link_dict)
        self.link_type = 'epics'


    def deserialize(self, link_dict):
        super().deserialize(link_dict)
        # FIXME: This should be removed. Epics ECS GW currently uses this
        #        instead of DataInfo for some reason.
        if 'secop datatype' in link_dict:
            self.secop_datatype = link_dict['secop datatype']
        # Setup Epics PV names
        if 'get reference' in link_dict:
            self.get_reference = link_dict['get reference']
        if 'set reference' in link_dict:
            self.set_reference = link_dict['set reference']
        # Verify values and set defaults for missing values if possible
        if not self.get_reference:
            raise secop_errors.InternalError(f"lib.Link: EPICS link is missing mandatory get-reference. {link_dict}")
        if not self.readonly and not self.set_reference:
            raise secop_errors.InternalError(f"lib.Link: Writable EPICS link is missing set-reference. {link_dict}")
        if not self.set_reference:
            self.set_topic = None
        self.reply = None


    def __eq__(self, other):
        """ EPICSLinks are equal if they have the same topics an references"""
        if self.value_topic != other.value_topic:
            return False
        if self.get_topic != other.get_topic:
            return False
        if self.set_topic != other.set_topic:
            return False
        if self.get_reference != other.get_reference:
            return False
        if self.set_reference != other.set_reference:
            return False
        return True


    def serialize(self):
        structure = super().serialize()
        structure['get reference'] = self.get_reference
        structure['set reference'] = self.set_reference
        return structure


    def create_references_from_topics(self, separator=EPICS_PV_NAME_SEPARATOR):
        """ If references does not exist in the link get_reference will be created from value_topic and if a set_topics
        exist a set_reference will be created from that the / in the topic will be substituted for <separator>"""
        if not self.get_reference:
            self.get_reference = self.value_topic
            self.get_reference = self.get_reference.replace('/', separator)
        if not self.set_reference:
            if self.set_topic:
                self.set_reference = self.set_topic
                self.set_reference = self.set_topic.replace('/', separator)
            else:
                print(self.set_topic)
        return self


    def colonize(self):
        """ If PV name is created from the value/set topics, the separator need to be  replaced from EPICS naming"""
        if not self.get_reference:
            self.get_reference = self.value_topic
        self.get_reference = self.get_reference.replace('/', EPICS_PV_NAME_SEPARATOR)
        if self.set_reference:
            self.set_reference = self.set_reference.replace('/', EPICS_PV_NAME_SEPARATOR)
        elif self.set_topic: #New, can be nicer....
            self.set_reference = self.set_topic.replace('/', EPICS_PV_NAME_SEPARATOR)


if __name__ == '__main__':
    link1 = EpicsLink({'value topic': 'test1', 'get reference': 'test1_reference', 'readonly': True})
    print(link1)
