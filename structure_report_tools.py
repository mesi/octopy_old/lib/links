from ..secop import errors as secop_errors
from ..secop import datainfo
from .epics_ecs_link import *
from .epics_ecs_link import EpicsECSLink as EpicsLink
from .links import LinkList

MANDATORY_NODE_KEYS = ('equipment_id', 'description')
OPTIONAL_NODE_KEYS = ('firmware', 'implementor', 'timeout')
MANDATORY_MODULE_KEYS = ('description', 'interface_classes')
OPTIONAL_MODULE_KEYS = ('visibility', 'description')
MANDATORY_ACCESSIBLE_KEYS = ('description', 'datainfo', 'readonly')
OPTIONAL_ACCESSIBLE_KEYS = ('equipment_id', 'description')
ECS_LINK_NAME = 'links'


def rename_key(old_key, new_key, structure_as_dict):
    ret = dict()
    for k, v in structure_as_dict.items():
        k = k if not k == old_key else new_key
        if isinstance(v, dict):
            ret[k] = rename_key(old_key, new_key, v)
        else:
            ret[k] = v
    return ret


def validate_structure(input_structure):
    """ The structure report used on octopus has more information than the structure report defined by SECoP has.
    This function takes an extended structure from Octopus and returns a structure with octopus specific information
    like links.
    It also adds:
    readonly, if missing on the accessible level
    interface_classes if missing on module level
    """

    if not isinstance(input_structure, dict):
        raise TypeError('structure should be a dict')
    validated_structure = dict()
    validated_structure['modules'] = dict()
    if input_structure.get('modules'):
        if not isinstance(input_structure['modules'], dict):
            raise TypeError('modules should be stored as a dict')
    else:
        raise secop_errors.SecopException('modules key missing')
    for node_key in MANDATORY_NODE_KEYS:
        try:
            validated_structure[node_key] = input_structure[node_key]
        except KeyError as err2_info:
            raise secop_errors.SecopException('Missing mandatory key/value-pair: {} at node level'.format(err2_info))
    for node_key in OPTIONAL_NODE_KEYS:
        try:
            validated_structure[node_key] = input_structure[node_key]
        except KeyError:
            pass
    for module_name in input_structure.get('modules'):
        current_module = input_structure['modules'].get(module_name)
        new_module = dict()
        new_module['accessibles'] = dict()
        for module_key in MANDATORY_MODULE_KEYS:
            if current_module.get(module_key):
                new_module[module_key] = current_module[module_key]
            else:
                if module_key == 'interface_classes':
                    new_module['interface_classes'] = ['readable']
                else:
                    raise secop_errors.SecopException('Missing mandatory key/value-pair: {} in module {}'
                                                      .format(module_key, module_key))
        for module_key in current_module.keys():
            if module_key not in new_module.keys():
                new_module[module_key] = current_module[module_key]
        for accessible_name in current_module.get('accessibles'):
            current_accessible = current_module['accessibles'].get(accessible_name)
            new_accessible = dict()
            for accessible_key in MANDATORY_ACCESSIBLE_KEYS:
                if accessible_key == 'readonly':
                    if 'readonly' not in current_accessible.keys():
                        readonly = True
                        hw_link = current_accessible.get(ECS_LINK_NAME)
                        if hw_link:
                            if hw_link.get('set topic'):
                                readonly = False
                        ecs_link = current_accessible.get(ECS_LINK_NAME)
                        if ecs_link:
                            if ecs_link.get('set topic'):
                                readonly = False
                        current_accessible['readonly'] = readonly
                        continue
                    else:
                        readonly = current_accessible.get('readonly')

                    if accessible_key == 'octopy':
                        for octopy_key in current_accessible.get('octopy').keys():
                            hw_link = current_accessible.get(ECS_LINK_NAME)
                            if octopy_key == hw_link:
                                if not hw_link.get('value topic'):
                                    raise secop_errors.error_missing_mandatory('value topic')
                                if readonly is False:
                                    if not hw_link.get('set topic'):
                                        hw_link['set topic'] = '{}/set'.format(hw_link('value topic'))
                                    if not hw_link.get('set reference'):
                                        hw_link['set reference'] = '{}:{}:{}'.format(input_structure.get('equipment_id'),
                                                                                     module_name, accessible_name)
                                    current_accessible[ECS_LINK_NAME] = hw_link
                            ecs_link = current_accessible.get(ECS_LINK_NAME)
                            if octopy_key == ecs_link:
                                if not ecs_link.get('value topic'):
                                    raise secop_errors.error_missing_mandatory('value topic')
                                if readonly is False:
                                    if not ecs_link.get('set topic'):
                                        ecs_link['set topic'] = '{}/set'.format(ecs_link.get('value topic'))
                                    if not ecs_link.get('set reference'):
                                        ecs_link['set reference'] = '{}:{}:{}'.format(input_structure.get('equipment_id'),
                                                                                      module_name, accessible_name)
                                    current_accessible[ECS_LINK_NAME] = ecs_link

                if current_accessible.get(accessible_key) is not None:
                    new_accessible[accessible_key] = current_accessible[accessible_key]
                else:
                    raise secop_errors.SecopException('Missing mandatory key/value pair {} in {}:{}'
                                                      .format(accessible_key, module_name, accessible_name))
            for accessible_key in current_accessible.keys():
                if accessible_key not in new_accessible.keys():
                    if accessible_key in (ECS_LINK_NAME, ECS_LINK_NAME):
                        lnk = EpicsLink(**current_accessible.get(accessible_key))
                        new_accessible[accessible_key] = lnk.structure()
                    else:
                        try:
                            new_accessible[accessible_key] = current_accessible[accessible_key]
                        except KeyError:
                            pass
            try:
                new_module['accessibles'][accessible_name] = new_accessible
            except KeyError:
                pass
        validated_structure['modules'][module_name] = new_module
    return validated_structure


def make_hardware_link_list(structure, remove=False):
    """ Function used to extract link-lists for the hardware side"""
#    structure = validate_structure(structure)
    link_dict = {}
    for module in structure['modules'].keys():
        lnk_lst = None
        if module != '_order':
            link_list = LinkList()
            for accessible in structure['modules'][module]['accessibles']:
                if accessible != '_order':
                    if ECS_LINK_NAME in structure['modules'][module]['accessibles'][accessible].keys():
                        try:
                            lnk_lst = EpicsLink(**structure['modules'][module]['accessibles'][accessible][ECS_LINK_NAME])
                        except TypeError as err4_info:
                            print(err4_info)
                        if remove:
                            del(structure['modules'][module]['accessibles'][accessible][ECS_LINK_NAME])
                    elif structure['modules'][module]['accessibles'][accessible]["datainfo"]["type"] == "command":
                        print("{}:{} is a command and should have a HWGW".format(module, accessible))
                    else:

                        raise KeyError('Accessible "{}" in module "{}" does not have a {}-key'.format(accessible,
                                                                                                      module,
                                                                                                      ECS_LINK_NAME))
                    if lnk_lst:
                        link_list.update(lnk_lst)
            link_dict[module] = link_list
    return link_dict



def make_ecs_link_list(structure, reference_separator=EPICS_PV_NAME_SEPARATOR):
    """ Function used to extract link-lists from structure reports for the ECS side.
     It first looks for an ECS-link, if no ECS-link exists for that accessible it creates one from the HW-link"""
#    structure = validate_structure(structure)
    link_list = LinkList(link_type=EpicsLink)
    if not isinstance(structure['modules'], dict):
        raise AttributeError('Modules has to be dictionaries')
    for module in structure['modules'].keys():
        for accessible in structure['modules'][module]['accessibles']:
            if ECS_LINK_NAME in structure['modules'][module]['accessibles'][accessible]['octopy'].keys():
                val_t = structure['modules'][module]['accessibles'][accessible]['octopy'][ECS_LINK_NAME].get('value topic')
                set_t = structure['modules'][module]['accessibles'][accessible]['octopy'][ECS_LINK_NAME].get('set topic')
                sdt = structure['modules'][module]['accessibles'][accessible].get('datainfo')
                ref = structure.get('equipment_id')
                if not ref:
                    raise secop_errors.BadJSON('The "equipment_id" key is missing')
                if accessible != "value":
                    ref += EPICS_PV_NAME_SEPARATOR + module + "-" + accessible  # EPICS_PV_NAME_SEPARATOR + accessible
                else:
                    ref += EPICS_PV_NAME_SEPARATOR + module # EPICS_PV_NAME_SEPARATOR + accessible
                if not structure['modules'][module]['accessibles'][accessible]['octopy'][ECS_LINK_NAME].get('get reference'):
                    ref = str(structure.get('equipment_id'))
                    if not ref:
                        raise secop_errors.BadJSON('The "equipment_id" key is missing')
                    if accessible != "value":
                        ref += EPICS_PV_NAME_SEPARATOR + module + "-" + accessible  # EPICS_PV_NAME_SEPARATOR + accessible
                    else:
                        ref += EPICS_PV_NAME_SEPARATOR + module  # EPICS_PV_NAME_SEPARATOR + accessible
                    sr = None
                    readonly = structure['modules'][module]['accessibles'][accessible].get('readonly')
                    if readonly is not None:
                        sr = ref #+ '-set'
                    else:
                        secop_errors.ProtocolErrorError('readable not set: {} in module {}'.format(accessible, module))
                    link_dict = dict()
                    # link_dict = structure['modules'][module]['accessibles'][accessible]['octopy'][ECS_LINK_NAME]
#                    link_dict['secop datatype'] = datainfo.from_structure(sdt)
                    link_dict['secop datatype'] = datainfo.from_structure(sdt)
                    link_dict['datainfo'] = sdt
                    link_dict["value topic"] = val_t
                    link_dict["get reference"] = ref + "-R"  #  Only fix for this JULABO integration
                    link_dict["readonly"] = readonly
                    link_dict["description"] = structure['modules'][module]['accessibles'][accessible].get('description')


                    if not readonly:
                        link_dict["set topic"] = set_t
                        link_dict["set reference"] = sr + "-S"

                lnk_lst = EpicsLink(link_dict=link_dict)
            else:
                raise KeyError('Accessible "{}" in module "{}" does not have a {} -key'
                               .format(accessible, module, ECS_LINK_NAME))
            lnk_lst.module = module
            lnk_lst.accessible = accessible
            if reference_separator:
                lnk_lst.create_references_from_topics(reference_separator)
            link_list.update(lnk_lst)
    return link_list



def make_ecs_link_list_old(structure, reference_separator=EPICS_PV_NAME_SEPARATOR):
    """ Function used to extract link-lists from structure reports for the ECS side.
     It first looks for an ECS-link, if no ECS-link exists for that accessible it creates one from the HW-link"""
#    structure = validate_structure(structure)
    link_list = LinkList(link_type=EpicsLink)
    if not isinstance(structure['modules'], dict):
        raise AttributeError('Modules has to be dictionaries')
    for module in structure['modules'].keys():
        for accessible in structure['modules'][module]['accessibles']:
            if ECS_LINK_NAME in structure['modules'][module]['accessibles'][accessible]['octopy'].keys():
                sdt = structure['modules'][module]['accessibles'][accessible].get('datainfo')
                if not structure['modules'][module]['accessibles'][accessible]['octopy'][ECS_LINK_NAME].get('get reference'):
                    ref = str(structure.get('equipment_id'))
                    if not ref:
                        raise secop_errors.BadJSON('The "equipment_id" key is missing')
                    ref += EPICS_PV_NAME_SEPARATOR + module + EPICS_PV_NAME_SEPARATOR + accessible
                    sr = None
                    readonly = structure['modules'][module]['accessibles'][accessible].get('readonly')
                    if readonly is not None:
                        sr = ref + ':set'
                    else:
                        secop_errors.ProtocolErrorError('readable not set: {} in module {}'.format(accessible, module))
                if not structure['modules'][module]['accessibles'][accessible]['octopy'][ECS_LINK_NAME].get('value topic'):
                    try:
                        structure['modules'][module]['accessibles'][accessible]['octopy'][ECS_LINK_NAME]['value topic'] =\
                            structure['modules'][module]['accessibles'][accessible]['octopy'][ECS_LINK_NAME].get('value topic')
                    except KeyError as hw_link_err:
                        print('Keyerror on {}:{} error:{}'.format(module, accessible, hw_link_err))
                link_dict = structure['modules'][module]['accessibles'][accessible]['octopy'][ECS_LINK_NAME]
                link_dict['secop_datatype'] = datainfo.from_structure(sdt)
                lnk_lst = EpicsLink(link_dict=link_dict)
            elif ECS_LINK_NAME in structure['modules'][module]['accessibles'][accessible]['octopy'].keys():
                val_t = structure['modules'][module]['accessibles'][accessible]['octopy'][ECS_LINK_NAME].get('value topic')
                set_t = structure['modules'][module]['accessibles'][accessible]['octopy'][ECS_LINK_NAME].get('value topic')
                sdt = structure['modules'][module]['accessibles'][accessible].get('datainfo')
                print(sdt)
                ref = structure.get('equipment_id')
                if not ref:
                    raise secop_errors.BadJSON('The "equipment_id" key is missing')
                ref += EPICS_PV_NAME_SEPARATOR + module + EPICS_PV_NAME_SEPARATOR + accessible
                sr = None
                if set_t:
                    sr = ref + ':set'
                lnk_lst = EpicsLink({"ecs link": True, "value topic": val_t, "set topic": set_t, "get reference": ref,
                                    "set reference": sr, "secop datatype": datainfo.from_structure(sdt)})
            else:
                raise KeyError('Accessible "{}" in module "{}" does not have a {} or {} -key'
                               .format(accessible, module, ECS_LINK_NAME, ECS_LINK_NAME))
            lnk_lst.module = module
            lnk_lst.accessible = accessible
            if reference_separator:
                lnk_lst.create_references_from_topics(reference_separator)
            link_list.update(lnk_lst)
    return link_list


def clean_describe(extended_structure):
    """ The structure report used on octopus has more information than the structure report defined by SECoP has.
    This function takes an extended structure from Octopus and returns a structure without octopus specific information
    like links"""
    if not isinstance(extended_structure, dict):
        raise TypeError('structure should be a dict')
    plain_structure = dict()
    plain_structure['modules'] = dict()
    if extended_structure.get('modules'):
        if not isinstance(extended_structure['modules'], dict):
            raise TypeError('modules should be stored as a dict')
    else:
        secop_errors.error_missing_mandatory('modules', 'node')
        return None
    for node_key in MANDATORY_NODE_KEYS:
        try:
            plain_structure[node_key] = extended_structure[node_key]
        except KeyError as err2_info:
            secop_errors.error_missing_mandatory(err2_info)
            return None
    for node_key in OPTIONAL_NODE_KEYS:
        try:
            plain_structure[node_key] = extended_structure[node_key]
        except KeyError:
            pass
    for module_name in extended_structure.get('modules'):
        current_module = extended_structure['modules'].get(module_name)
        new_module = dict()
        new_module['accessibles'] = dict()
        for module_key in MANDATORY_MODULE_KEYS:
            try:
                new_module[module_key] = current_module[module_key]
            except KeyError as err1_info:
                secop_errors.error_missing_mandatory(err1_info, 'module')
                return None
        for module_key in OPTIONAL_MODULE_KEYS:
            try:
                new_module[module_key] = current_module[module_key]
            except KeyError:
                pass
        for accessible_name in current_module.get('accessibles'):
            current_accessible = current_module['accessibles'].get(accessible_name)
            new_accessible = dict()
            for accessible_key in MANDATORY_ACCESSIBLE_KEYS:
                try:
                    new_accessible[accessible_key] = current_accessible[accessible_key]
                except KeyError as error_info:
                    secop_errors.error_missing_mandatory(error_info, module_name + ':' + accessible_name)
                    return None
            for accessible_key in OPTIONAL_ACCESSIBLE_KEYS:
                try:
                    new_accessible[accessible_key] = current_accessible[accessible_key]
                except KeyError:
                    pass
            try:
                new_module['accessibles'][accessible_name] = new_accessible
            except KeyError:
                pass
        plain_structure['modules'][module_name] = new_module
    return plain_structure


if __name__ == '__main__':
    import paho.mqtt.subscribe as subscribe
    import json

    structure_json = '{"description":"Demo SES for showing and testing the capability of Octopy","equipment_id":"test1","modules":{"plc_knobs":{"description":"PLC Knobs","interface_classes":["Knobs","Analog Inputs","Readable"],"accessibles":{"knob_1":{"description":"Knob 1","readonly":true,"datainfo":{"type":"double","fmtstr":"%.1f","min":0,"max":100,"unit":"%"},"octopy":{"links":{"get topic":"octopy-demo-plc/knob_1/value/get","value topic":"octopiiiy-demo-plc/knob_1/value"},"hints":{"ui":{"widgets":["RadialGaugeWidget"]}}}},"knob_2":{"description":"Knob 2","readonly":true,"datainfo":{"type":"double","fmtstr":"%.1f","min":0,"max":100,"unit":"%"},"octopy":{"links":{"get topic":"octopy-demo-plc/knob_2/value/get","value topic":"octopy-demo-plc/knob_2/value"},"hints":{"ui":{"widgets":["RadialGaugeWidget"]}}}},"knob_3":{"description":"Knob 3","readonly":true,"datainfo":{"type":"double","fmtstr":"%.1f","min":0,"max":100,"unit":"%"},"octopy":{"links":{"get topic":"octopy-demo-plc/knob_3/value/get","value topic":"octopy-demo-plc/knob_3/value"},"hints":{"ui":{"widgets":["RadialGaugeWidget"]}}}},"knob_4":{"description":"Knob 4","readonly":true,"datainfo":{"type":"double","fmtstr":"%.1f","min":0,"max":100,"unit":"%"},"octopy":{"links":{"get topic":"octopy-demo-plc/knob_4/value/get","value topic":"octopy-demo-plc/knob_4/value"},"hints":{"ui":{"widgets":["RadialGaugeWidget"]}}}},"knob_5":{"description":"Knob 5","readonly":true,"datainfo":{"type":"double","fmtstr":"%.1f","min":0,"max":100,"unit":"%"},"octopy":{"links":{"get topic":"octopy-demo-plc/knob_5/value/get","value topic":"octopy-demo-plc/knob_5/value"},"hints":{"ui":{"widgets":["RadialGaugeWidget"]}}}}},"visibility":"user"},"plc_buttons":{"description":"PLC Buttons","interface_classes":[""],"accessibles":{"button_1":{"description":"Button 1","readonly":false,"datainfo":{"type":"bool"},"octopy":{"links":{"set topic":"octeeeeopy-demo-plc/button_1/set","value topic":"octopy-demo-plc/button_1/value"},"hints":{"ui":{"widgets":["ToggleWidget"]}}}},"button_2":{"description":"Button 2","readonly":false,"datainfo":{"type":"bool"},"octopy":{"links":{"set topic":"octopy-demo-plc/button_2/set","value topic":"octopy-demo-plc/button_2/value"},"hints":{"ui":{"widgets":["ToggleWidget"]}}}},"button_3":{"description":"Button 3","readonly":false,"datainfo":{"type":"bool"},"octopy":{"links":{"set topic":"octopy-demo-plc/button_3/set","value topic":"octopy-demo-plc/button_3/value"},"hints":{"ui":{"widgets":["ToggleWidget"]}}}},"button_4":{"description":"Button 4","readonly":false,"datainfo":{"type":"bool"},"octopy":{"links":{"set topic":"octopy-demo-plc/button_4/set","value topic":"octopy-demo-plc/button_4/value"},"hints":{"ui":{"widgets":["ToggleWidget"]}}}}},"visibility":"user"},"channel_a":{"description":"Channel A","interface_classes":[""],"accessibles":{"temp":{"description":"Temperature Temperature for channel A","readonly":true,"datainfo":{"type":"string"},"octopy":{"links":{"get topic":"LS336/channel/A/temp/get","value topic":"LS336/channel/A/temp"},"hints":{"ui":{"widgets":["StringWidget"]}}}},"name":{"description":"Name Channel name","readonly":false,"datainfo":{"type":"string"},"octopy":{"links":{"get topic":"LS336/channels/A/name/value/get","set topic":"LS336/channels/A/name/set","value topic":"LS336/channels/A/name/value"},"hints":{"ui":{"widgets":["StringWidget"]}}}}},"visibility":"user"}},"firmware":"2021.03.05","implementor":"ESS","timeout":"10"}'

    msg = subscribe.simple(topics='se-octopy-demo/octopy/structure', hostname='se-octopy-demo.cslab.esss.lu.se', port=1883,
                           auth={'username': 'mesi', 'password': 'vaha23neca'})
#    msg = subscribe.simple(topics='rig/octopy/structure', hostname='172.30.244.210', port=1883,
#                     auth={'username': 'mesi', 'password': 'vaha23neca'})
    print('Got structure')
    structure = validate_structure(json.loads(msg.payload))
    print(json.dumps(structure, indent=4))
#    structure = validate_structure(json.loads(structure_json))

    # print(json.dumps(structure, indent=4))
    lnklst = make_ecs_link_list(structure)
    for link in lnklst:
        print(link.as_dict())
    print(lnklst.linklist_as_json())

