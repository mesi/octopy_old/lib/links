from os import path
import json
from ..secop import errors as secop_errors
from ..secop import datainfo

class BaseLink:
    """ ____PROPERTIES____:

    value_topic (mandatory)
        the topic where the data for the accessible is published.
        If no get_topic is supplied /get will be appended automatically.
        If accessible is a writable(eg. it has a write reference) set_topic will be <value_topic>/set
        set, get and value can all be overridden by explicitly setting them: Never the less auto_topic HAS to be set
        because it is used as an identifier when link exists as json objects.

    channel_id
        If not provided it will be set to value_topic

    description
        Describe the data that it transferred on this topic(s)

    get_topic
        the topic where you poll for data, if polling is needed

    set_topic
        the topic for issuing a command or setting a parameter.

    unit
        Unit of the data.

     """

    def __init__(self, link_dict, **kwargs):
        self.value_topic = None
        self.channel_id = None
        self.get_topic = None
        self.set_topic = None
        self.readonly = True
        self.description = None
        self.unit = None
        self.datainfo = None
        self.unit = None
        self.link_type = 'base'
        self.datasource_id = 'mqtt'
        self.deserialize(link_dict)


    def deserialize(self, link_dict):
        if not isinstance(link_dict, dict):
            raise TypeError(f"Link configuration must be a dict. Got {link_dict.__class__.__name__}.")
        if 'value topic' in link_dict:
            self.value_topic = link_dict['value topic']
        if not self.value_topic:
            raise AttributeError("Can't deserialize link configuration - mandatory key 'value topic' is missing or has empty value.")
        if 'id' in link_dict:
            self.channel_id = link_dict['id']
        else:
            self.channel_id = self.value_topic
        if link_dict.get('get topic'):
            self.get_topic = link_dict['get topic']
        if link_dict.get('set topic'):
            self.set_topic = link_dict['set topic']
        if 'readonly' in link_dict:
            # Guarantee that readonly is set to True/False
            self.readonly = True if link_dict['readonly'] else False
            if not self.readonly and (not hasattr(self, 'set_topic') or not self.set_topic):
                self.set_topic = self.value_topic + '/set'
        elif self.set_topic:
            self.readonly = False
        else:
            self.readonly = True
        # Set default get topic if it's missing
        #if not self.get_topic:
        #    self.get_topic = path.join(self.value_topic, 'get')
        if 'unit' in link_dict:
            self.unit = link_dict['unit']
        # TODO: Data source id should probably be set by the datasources on
        # export instead of here
        if 'datasource id' in link_dict:
            self.datasource_id = link_dict['datasource id']
        if 'datainfo' in link_dict:
            self.datainfo = datainfo.from_structure(link_dict['datainfo'])
        if 'description' in link_dict:
            self.description = link_dict['description']


    def update(self, link_dict):
        self.deserialize(link_dict)


    def serialize(self):
        structure = {
            'value topic': self.value_topic,
            'get topic': self.get_topic,
            'set topic': self.set_topic,
            'description': self.description,
            'readonly': self.readonly,
            'unit': self.unit,
            'link type': self.link_type,
            'channel id': self.channel_id,
            'datasource id': self.datasource_id
        }
        if self.datainfo:
            structure['datainfo'] = self.datainfo.serialize()
        return structure


    def as_dict(self):
        return self.serialize()


    def structure(self):
        return self.serialize()


    def __str__(self):
        return str(self.serialize())

    def as_json(self):
        return json.dumps(self.serialize(), indent=4)

    def __eq__(self, other):
        """ BaseLinks are equal if they have the same topics"""
        if self.value_topic != other.value_topic:
            return False
        if self.get_topic != other.get_topic:
            return False
        if self.set_topic != other.set_topic:
            return False
        return True


    def set_datainfo(self, value):
        """ Sets the datainfo by analysing data type of the supplied value"""
        self.datainfo = datainfo.suggest(value, parse_from_string=True)


    def update_link(self, link):
        self.set_topic = link.set_topic
        self.get_topic = link.get_topic
        if link.description:
            self.description += "\n" + link.description


    def update_from_dict(self, update_dict):
        self.deserialize(update_dict)


if __name__ == '__main__':
    link1 = BaseLink({'value topic': 'test1'})
    print(link1)
    link2 = BaseLink({'value topic': 'test1', 'get topic': 'test1/get'})
    link3 = BaseLink({'value topic': 'test1'})
    print(link1 == link2)
    print(link1 == link3)
