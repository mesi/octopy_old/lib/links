import json
import logging
from .base_link import BaseLink
from ..secop import errors as secop_errors


def channel_list(dict_of_linklists):
    """ Input a dict of linklists where the key is the hardware id, returns a channel list"""
    ret = list()
    for k, v in dict_of_linklists.items():
        lnk = v
        lnk.append({'hardware id': k})
        ret.append(lnk)


class LinkList:
    """ LinkList implement many of Python List functions and
    next_prioritized: for eg returning a prioritized link for a serial gateways where bandwidth is limiting
    find_by_<property> unambiguous searching for links depending on property
    """
    def __init__(self, link_type):
        self.links = dict()
        self.link_type = link_type
        self.next = 0
        self.last_priority = 0
        self._got_all_datainfos = True

    def __getitem__(self, item):
        try:
            return self.links[item]
        except KeyError as error_info:
            raise secop_errors.InternalError('Not a valid value topic: {}'.format(error_info))

    def __setitem__(self, key, value):
        self.links[key] = value

    def __str__(self):
        lst = []
        for link in self.links.values():
            lst.append(link)
        return str(lst)

    def __iter__(self):
        self.n = 0
        return self

    def keys(self):
        return self.links.keys()

    def delete(self, other):
        if isinstance(other, self.link_type):
            del self.links[other.value_topic]
        elif isinstance(other, str):
            try:
                del self.links[other]
            except KeyError as err:
                return
        return self

    def add(self, new_link):
        if isinstance(new_link, self.link_type):
            logging.debug('In links.py linklist.add() adding: {}'.format(new_link))
            if new_link.value_topic not in self.links.keys():
                self.links[new_link.value_topic] = new_link
            else:
                self.links[new_link.value_topic].update_link(new_link)
        else:
            raise TypeError('object is not a Link')
        if new_link.datainfo:
            self._got_all_datainfos = True
        else:
            self._got_all_datainfos = False
        return self


    def remove(self, link_id):
        if link_id in self.links:
            del self.links[link_id]
        else:
            raise KeyError(f"LinkList has no such link: \"{link_id}\"")


    def __next__(self):
        if self.n < len(self.links):
            ret = list(self.links.values())[self.n]
            self.n += 1
            return ret
        else:
            raise StopIteration

    def __len__(self):
        return len(self.links)

    def append(self, link):
        if isinstance(link, self.link_type):
            self.add(link)
        else:
            raise TypeError('object is not a Link')

    def update(self, links):
        if isinstance(links, LinkList):
            self.links.update(links.links)
        elif isinstance(links, self.link_type):
            self.add(links)
        else:
            raise TypeError('{} is not a Link or LinkList'.format(type(links)))

    def list_topics(self):
        """ List all topics in linklist"""
        ret = list()
        for link in self.links.values():
            ret.extend(link.list_topics())
        return ret

    def datainfos(self):
        if not self._got_all_datainfos:
            for lnk in self.links.values():
                if not lnk.datainfo:
                    return lnk
        return None

    def next_prioritized(self):
        if len(self.links) == 0:
            raise secop_errors.InternalError('Cant use next_prioritized when LinkList is empty')
        for a in range(len(self.links)):
            if self.next >= len(self.links):
                self.next = 0
            current_link = list(self.links.values())[self.next]
            if current_link.get_reference:
                if current_link.skipped >= current_link.priority and current_link.priority + self.last_priority <= 4:
                    current_link.skipped = 0
                    self.last_priority = current_link.priority
                    ret = list(self.links.values())[self.next]
                    self.next += 1
                    return ret
                else:
                    current_link.skipped += 1
                self.next += 1
                return self.next_prioritized()
            else:
                print('get_reference missing')
            self.next += 1

    def find_by_get_reference(self, reference):
        reference = reference.strip()
        for link in self.links.values():
            if link.get_reference == reference:
                return link
        return None

    def find_by_set_reference(self, reference):
        reference = reference.strip()
        for link in self.links.values():
            if link.set_reference == reference:
                return link
        return None

    def find_by_value_topic(self, value_topic):
        if isinstance(value_topic, str):
            return self.links.get(value_topic)
        else:
            raise TypeError('object is not a value_topic')

    def find_by_set_topic(self, set_topic):
        for link in self.links.values():
            if link.set_topic == set_topic:
                return link
        return None

    def find_by_get_topic(self, get_topic):
        for link in self.links.values():
            if link.get_topic == get_topic:
                return link
        return None

    def add_link(self, link_dict):
        """Add a link to the Linklist. If the value_topic already exists in the link list do nothing and return None"""
        if isinstance(link_dict, dict):
            logging.debug('In links.py linklist.add_link() adding: {}'.format(link_dict))
            new_link = self.link_type(link_dict)
            self.add(new_link)
            return new_link
        else:
            return None
#            raise secop_errors.InternalError('When adding a link it should be in form of a dict')


    def add_links_from_json_array(self, json_array):
        """Adds links from JSON array, returns list of link additions that was possible make"""
        lnks = []
        link_dict_list = json.loads(json_array)
        if isinstance(link_dict_list, list):
            for link_dict in link_dict_list:
                logging.debug('In links.py linklist.add_link_from_json() adding: {}'.format(link_dict))
                added_link = self.add_link(link_dict)
                if not added_link:
                    raise secop_errors.InternalError('When adding a link it should be in form of a dict')
                if added_link:
                    lnks.append(added_link)
        return lnks

    def add_links_from_list(self, link_dict_list):
        """Adds links from list, returns list of link additions that was possible make"""
        lnks = []
        for link_dict in link_dict_list:
            if isinstance(link_dict, dict):
                added_link = self.add_link(link_dict)
                if not added_link:
                    print("Something is wrong, all links should be added. All previous stuff overwritten/removed")
                else:
                    if added_link:
                        lnks.append(added_link)
        return lnks


    def linklist_as_list(self):
        structure = self.serialize()
        links = structure['links']
        return links


    def linklist_as_json(self):
        structure = self.serialize()
        links = structure['links']
        return json.dumps(links, indent=4)


    def serialize(self):
        structure = {
            'link_type': self.link_type.__class__.__name__
        }
        links = []
        for link in self.links.values():
            links.append(link.serialize())
        structure['links'] = links
        return structure


if __name__ == '__main__':
    import secop_datainfo.secop_datainfo as datainfo
    import epics_link
    """Usage example"""
    some_data_string1 = 'SETP A,99.99'
    some_regex_string1 = r"(?P<reference>[\w\s]+),(?P<value>[\d\.]+)"

    """LinkList"""
    print('_'*5 + ' Linklists' + '_'*5)
    my_link_list = LinkList(link_type=RequestResponseLink)
    my_link_list.add_links_from_json_array('[{"value topic":"temp","get reference":"IN_PV_00","set topic":"setpoint",'
                                           '"set reference":"OUT_SP_00"}]')
    my_link_list.add_links_from_json_array('[{"value topic":"temp2","get reference":"setpoint2"}]')
    my_link_list.add_links_from_json_array('[{"value topic":"temp3","get reference":"setpoint3", "priority": 4}]')
    my_link_list.add_links_from_json_array('[{"value topic":"temp4","get reference":"setpoint4"}]')
    my_link_list.add_links_from_json_array('[{"value topic":"temp5","get reference":"setpoint5","bajs":"kiss"}]')
    my_link_list2 = LinkList()

    print('printing link for link:')
    for lnk in my_link_list:
        print(lnk)
    print('done printing link for link:')

    """Single Link"""
    print('_'*5 + ' Single Link ' + '_'*5)
    my_dict = {"value topic": "temp19", "get reference": "setpoint19", "datainfo": datainfo.suggest(25.6).structure()}
    my_link_from_dict = BaseLink(my_dict)
    print('Link from dict: {}'.format(my_link_from_dict))
    my_update_dict = {"value topic": "temp20", "get reference": "setpoint202", "set reference": "tjohej"}
    my_link_from_dict.update_from_dict(my_update_dict)
    print('Updated Link from dict: {}'.format(my_link_from_dict))
    my_link = RequestResponseLink({'value topic': 'test1/hej/hopp', 'get reference': 'hejsan'})
    print(my_link)
    print(my_link.set_topic)

    """Adding RequestResponsLink-linklist"""
    print('_'*5 + ' Adding Link ' + '_'*5)
    my_link_list2.add(my_link)
    my_link_list2.add_links_from_json_array('[{"value topic":"temp5","get reference":"setpoint5"}, '
                                            '{"value topic":"temp6","get reference":"setpoint6"},'
                                            '{"value topic":"temp7","get reference":"setpoint7"},'
                                            '{"value topic":"temp8","get reference":"setpoint8"}]')

    my_link_list3 = LinkList(link_type=RequestResponseLink)
    my_link_3 = RequestResponseLink({'value topic': 'test1/hej/hopp', 'get reference': 'hejsan'})
    """Adding Link-linklist"""
    print('_'*5 + ' Adding Link ' + '_'*5)
    my_link_list3.add(my_link_3)
    my_link_list3.add_links_from_json_array('[{"value topic":"temp5","get reference":"setpoint5"}, '
                                            '{"value topic":"temp6","get reference":"setpoint6"},'
                                            '{"value topic":"temp7","get reference":"setpoint7"},'
                                            '{"value topic":"temp8","get reference":"setpoint8"}]')



    """Combining LinkLists"""
    print('_'*5 + ' Combining LinkLists ' + '_'*5)
    my_link_list3 = LinkList()
    my_link_list3.update(my_link_list)
    my_link_list3.update(my_link_list2)
    print(my_link_list3.linklist_as_json())

    """Comparing LinkLists"""
    print('_'*5 + ' Comparing LinkLists ' + '_'*5)
    print('LinkLists are the same? {}'.format(my_link_list == my_link_list2))

    my_link_list.add_links_from_json_array('[{"value topic":"test1/hejsan/hoppsan", "get reference": "qwerty"}]')

    """Appending Link"""
    print('_'*5 + ' Appending Link ' + '_'*5)
    my_link_list.append(RequestResponseLink({'value topic': 'test1/hej/hopp2', 'get reference': 'setpoint5/tjosan/hejsan2',
                             'description': 'link to append'}))

    """Appending duplicate Link"""
    my_link_list.append(RequestResponseLink({'value topic': 'test1/hej/hopp2', 'get reference': 'setpoint5/tjosan/hejsan2',
                             'description': 'appending same link again'}))

    """Indexing LinkList"""
    print('_'*5 + ' Indexing LinkLists ' + '_'*5)
    print("link 2")
    try:
        print(my_link_list[2])
    except secop_errors.InternalError as err_info:
        print('SEcop_error: {}'.format(err_info))

    """Find and remove Link from LinkList"""
    print('_'*5 + ' Find and remove Link from LinkList ' + '_'*5)
    print(my_link_list)
    print('The link with value topic "temp3": {}'.format(my_link_list.find_by_value_topic('temp3')))
    my_link_list.update(my_link_list.find_by_value_topic('temp3'))

    print(my_link_list.find_by_value_topic('test1/hejsan/hopp2'))

    mqtt_link = BaseLink({'value topic': 'test_value', 'set topic': 'test_set', 'description': 'This is a test for MQTT hardware'})
    print(mqtt_link)
    print(json.dumps(my_link_list.list_topics()))
    print('as_json:')
    print(my_link_list.linklist_as_json())
    print(my_link_list.find_by_value_topic('test1/hej/hopp2').description)

    di = my_link_list.datainfos()
    while di:
        print('_________________________no datainfo:')
        print(di)
        di.datainfo = datainfo.suggest(5687.34526)
        print(di)
        di = my_link_list.datainfos()
    for ln in my_link_list:
        print(ln.get_reference)

    print("Next_prioritized, polling hardware 20 times depending on priority:")
    for n in range(20):
        lnk = my_link_list.next_prioritized()
        print('Sending command: {} with priority: {} to hardware'.format(lnk.get_reference, lnk.priority))
        if not lnk.get_reference:
            print(lnk)
    ne_test = RequestResponseLink({'value topic': 'test', 'set reference': 'testset', 'get reference': 'test_get'})
    print('\nPrinting my_link_list')
    for ln in my_link_list:
        print(ln.value_topic)
    print('\nDeleting temp3')
    my_link_list.delete('temp3')
    print('\nPrinting my_link_list')
    for ln in my_link_list:
        print(ln.value_topic)

    print('ne_test')
    print(ne_test)
    print('ne_test: {}'.format(ne_test.set_reference))
    # test = 'nej'
    # if "temp3" in my_link_list:
    #     test = 'ja'

    print('listing topics {}'.format(my_link_list.list_topics()))
    mqtt_link2 = BaseLink({'value topic': 'test_value', 'description': 'This is a test for MQTT hardware'})
    print(mqtt_link2)
    print(mqtt_link2.update_from_dict(json.loads('{"channel id": "test", "get topic":"temp8","get reference":"setpoint8"}')))
    print(mqtt_link2.value_topic)
    print(mqtt_link2.__dict__)
    print(my_link_list.list_topics())

    e_l = epics_link.EpicsLink(link_dict={"value topic": "epicstest", "get reference": "SES:JULABO-001:TEMP", "set reference": "SES:JULABO-001:TEMP"})
    from request_response_link import RequestResponseLink
    rr_l = RequestResponseLink(link_dict={"value topic": "epicstest", "get reference": "SES:JULABO-001:TEMP", "set reference": "SES:JULABO-001:TEMP"})
    # epics_link_list = LinkList(link_type=epics_link.EpicsLink)
    # epics_link_list.add_links_from_json_array('[{"value topic":"epicstest", "get reference": "SES:JULABO-001:TEMP", "set reference": "SES:JULABO-001:TEMP"}]')
    # print(epics_link_list.find_by_get_reference("SES:JULABO-001:TEMP"))
    print('\n\n')
    print(e_l.as_dict())
    print('\n')
    print(rr_l.as_dict())

    rr_l.datainfo = datainfo.suggest(35.6)
    print(rr_l.as_dict())
